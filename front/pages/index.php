<?php
if (!isset($_COOKIE["type"])) {
    header('Location: login.php');
    exit();
} else if ($_COOKIE["type"] === "user") {
    header('Location: chat.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Author: Maksym Myna. Messaging system made for LPNU. Category: chatting">
    <meta name="theme-color" content="#000000" />
    <link rel="stylesheet" href="../styles/style.css">
    <link rel="stylesheet" href="../styles/header.css">
    <link rel="stylesheet" href="../styles/header_content.css">
    <link rel="stylesheet" href="../styles/popup.css">
    <link rel="stylesheet" href="../styles/admin_page.css">
    <link rel="manifest" href="../../manifest.json">
    <link rel="apple-touch-icon" href="../img/icon-192x192.png">
    <title>CMS</title>
</head>

<body>
    <header>

        <div id="nav_logo">
            <a class="nav-link" href="index.php">CMS</a>
        </div>

        <section id="nav_user">
            <div class="notification-icon">
                <svg class="bell" fill="#FFFFFF" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M10,21h4a2,2,0,0,1-4,0ZM3.076,18.383a1,1,0,0,1,.217-1.09L5,15.586V10a7.006,7.006,0,0,1,6-6.92V2a1,1,0,0,1,2,0V3.08A7.006,7.006,0,0,1,19,10v5.586l1.707,1.707A1,1,0,0,1,20,19H4A1,1,0,0,1,3.076,18.383ZM6.414,17H17.586l-.293-.293A1,1,0,0,1,17,16V10A5,5,0,0,0,7,10v6a1,1,0,0,1-.293.707Z" />
                    <circle class="blink" cx="17" cy="6" r="3" stroke="black" stroke-width="0" fill="red" />
                </svg>
            </div>

            <section id="notification-bar" class="not-visible">
                <div class="notification">
                    <span class="notification-sender">
                        <img src="../img/empty-pfp.webp" alt="profile picture" class="pfp">
                        <span class="notification-sender-name">admin</span>
                    </span>
                    <span class="notification-bubble">
                        <div class="notification-arrow"></div>
                        You have been registered!
                    </span>
                </div>
            </section>

            <span class="nav-link" id="to_profile"><img src="../img/empty-pfp.webp" alt="profile picture"
                    class="pfp"></span>
            <span id="nav_username">Maksym Myna</span>

            <section id="profile-header-bar" class="not-visible">
                <div class="to-profile">
                    <a href="chat.php">Profile</a>
                </div>
                <div class="log-out">
                    <text class="logout">Log Out</text>
                </div>

            </section>


        </section>

    </header>

    <section class="content">
        <nav>
            <ul>
                <li>
                    <a href="dashboard.html">Dashboard</a>
                </li>
                <li> <a href="index.php"><b>Students</b></a></li>
                <li> <a href="tasks.html">Tasks</a></li>
                <li>
                    <a href="chat.php">Chat</a>
                </li>

            </ul>
        </nav>


        <main>
            <table>
                <caption class="title">
                    <b>
                        Students
                    </b>
                </caption>
                <caption class="add_user">
                    <button class="user_adding">+</button>
                </caption>
                <thead>
                    <tr>
                        <th id="select_all" class="user_check">
                            <input type="checkbox" id="selectAll" name="select_all">
                        </th>
                        <label for="selectAll" class="visuallyhidden">Select all</label>
                        <th class="user_group">Group</th>
                        <th class="user_name">Name</th>
                        <th class="user_gender">Gender</th>
                        <th class="user_birthday">Birthday</th>
                        <th class="user_status">Status</th>
                        <th class="user_options">Options</th>
                    </tr>

                </thead>
                <tbody id="students-table-body">

                </tbody>
            </table>

            <section class="table_navigation">

                <svg class="disabled-svg" xmlns="http://www.w3.org/2000/svg" viewBox="12 12 24.5 24">
                    <path d="M12 36V12h3v24Zm22.35-.15-11.7-11.7 11.7-11.7 2.15 2.15-9.55 9.55 9.55 9.55Z"></path>
                </svg>
                <svg class="disabled-svg" xmlns="http://www.w3.org/2000/svg" viewBox="16 11.9 14.2 24.1">
                    <path d="M28.05 36 16 23.95 28.05 11.9l2.15 2.15-9.9 9.9 9.9 9.9Z"></path>
                </svg>
                <text class="current_tab">1</text>
                <svg class="disabled-svg" xmlns="http://www.w3.org/2000/svg" viewBox="16.6 11.9 14.2 24.1">
                    <path d="m18.75 36-2.15-2.15 9.9-9.9-9.9-9.9 2.15-2.15L30.8 23.95Z"></path>
                </svg>
                <svg class="disabled-svg" xmlns="http://www.w3.org/2000/svg" viewBox="11.45 12 24.55 24">
                    <path d="m13.6 35.75-2.15-2.15 9.6-9.6-9.6-9.6 2.15-2.15L25.35 24ZM33 36V12h3v24Z"></path>
                </svg>

            </section>
        </main>
    </section>
    <div class="popup_container not-visible">
        <div class="popup_container_content">

            <section class="top-popup">
                <span class="popup-title" id="popup-title">Add student</span>
                <button aria-label="close" class="exit-popup">
                    <svg id="close-adding" class="cross" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M19 5L4.99998 19M5.00001 5L19 19" stroke="#000000" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </button>
            </section>

            <section class="user_info">
                <section class="user-info-content">
                    <section class="user-info-texts">
                        <text>
                            <label for="group">
                                Group
                            </label>
                        </text>
                        <text>
                            <label for="first-name">First name</label>
                        </text>
                        <text>
                            <label for="last-name">Last name</label>
                        </text>
                        <text>
                            <label for="gender">Gender</label>
                        </text>
                        <text>
                            <label for="birthday">Birthday</label>
                        </text>
                    </section>

                    <form class="user-info-inputs" id="form">
                        <select name="group" id="group">
                            <option value="PZ-21">PZ-21</option>
                            <option value="PZ-22">PZ-22</option>
                            <option value="PZ-23">PZ-23</option>
                            <option value="PZ-24">PZ-24</option>
                            <option value="PZ-25">PZ-25</option>
                            <option value="PZ-26">PZ-26</option>
                        </select>
                        <input type="text" name="first_name" id="first-name" pattern="^\p{L}+$" required
                            placeholder="John">
                        <input type="text" name="last_name" id="last-name" pattern="^\p{L}+$" required
                            placeholder="Doe">
                        <select name="gender" id="gender">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="other">Other</option>
                        </select>
                        <input type="date" required name="birthday" id="birthday" min="1900-01-01">
                    </form>

                    <section id="inputs-check">
                    </section>

                </section>
            </section>


            <section class="bottom">
                <text id="filling-mistake">
                </text>
                <section class="buttons">
                    <button class="navigation-button" id="ok-add-user">Ok</button>
                    <button class="navigation-button" id="cancel-add-user">Cancel</button>
                </section>
            </section>
            </form>


        </div>
    </div>

    <div class="popup_container not-visible" id="remove-warning">
        <div class="popup_container_content" id="remove-warning-content">

            <section class="top-popup">
                <span class="popup-title">Warning</span>
                <button class="exit-popup" id="exit-deletion">
                    <svg aria-label="close" id="close-adding" class="cross" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path d="M19 5L4.99998 19M5.00001 5L19 19" stroke="#000000" stroke-width="2"
                            stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                </button>
            </section>

            <section class="user_info delete-info">
                <text class="warning-text">
                </text>
            </section>

            <section class="buttons">
                <button class="navigation-button" id="ok-delete-user">Ok</button>
                <button class="navigation-button" id="cancel-delete-user">Cancel</button>
            </section>
        </div>
    </div>


    <script src="../scripts/usersRequests.js"></script>
    <script src="../scripts/user.js"></script>
    <script src="../scripts/app.js"></script>
    <script src="../scripts/header.js"></script>
    <script src="../scripts/visibility.js"></script>
    <script src="../scripts/popUpHelper.js"></script>
    <script src="../scripts/validation.js"></script>
    <script src="../scripts/tablePaging.js"></script>
    <script src="../scripts/sw.js"></script>
</body>

</html>