<?php
if (isset($_COOKIE["type"])) {
    if ($_COOKIE["type"] === "admin")
        header('Location: index.php');
    if ($_COOKIE["type"] === "user")
        header('Location: index.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Author: Maksym Myna. Messaging system made for LPNU. Category: chatting">
    <meta name="theme-color" content="#000000" />
    <link rel="stylesheet" href="../styles/style.css">
    <link rel="stylesheet" href="../styles/header.css">
    <link rel="stylesheet" href="../styles/popup.css">
    <link rel="manifest" href="../../manifest.json">
    <link rel="apple-touch-icon" href="../img/icon-192x192.png">
    <title>CMS. Log In</title>
</head>

<body>
    <header>
        <section id="nav_logo">
            CMS
        </section>
    </header>
    <div class="popup_container">
        <div class="popup_container_content">
            <div class="user_info">
                <div class="user-info-content">
                    <form class="form">
                        <input type="text" name="login" id="login-signin" placeholder="login">
                        <input type="password" name="password" id="password-signin" placeholder="password">
                        <button class="button" id="login" type="submit">Log In</button>
                    </form>
                </div>
                <div class="login_error"></div>
            </div>
        </div>
    </div>
    <script src="../scripts/login.js"></script>
</body>

</html>