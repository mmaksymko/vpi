<?php
if (!isset($_COOKIE["type"])) {
    header('Location: login.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Author: Maksym Myna. Messaging system made for LPNU. Category: chatting">
    <meta name="theme-color" content="#000000" />
    <link rel="stylesheet" href="../styles/style.css">
    <link rel="stylesheet" href="../styles/header.css">
    <link rel="stylesheet" href="../styles/header_content.css">
    <link rel="stylesheet" href="../styles/chat.css">
    <link rel="stylesheet" href="../styles/popup.css">
    <link rel="manifest" href="../../manifest.json">
    <link rel="apple-touch-icon" href="../img/icon-192x192.png">
    <title>CMS</title>
</head>

<body>
    <header>
        <div id="nav_logo">
            <a class="nav-link" href="index.php">CMS</a>
        </div>
        <section id="nav_user">
            <div class="notification-icon">
                <svg class="bell" fill="#FFFFFF" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M10,21h4a2,2,0,0,1-4,0ZM3.076,18.383a1,1,0,0,1,.217-1.09L5,15.586V10a7.006,7.006,0,0,1,6-6.92V2a1,1,0,0,1,2,0V3.08A7.006,7.006,0,0,1,19,10v5.586l1.707,1.707A1,1,0,0,1,20,19H4A1,1,0,0,1,3.076,18.383ZM6.414,17H17.586l-.293-.293A1,1,0,0,1,17,16V10A5,5,0,0,0,7,10v6a1,1,0,0,1-.293.707Z" />
                    <circle class="blink" cx="17" cy="6" r="3" stroke="black" stroke-width="0" fill="red" />
                </svg>
            </div>

            <section id="notification-bar" class="not-visible">
                <div class="notification">
                    <span class="notification-sender">
                        <a href="index.php" class="notification-pfp"></a>
                        <img src="../img/empty-pfp.webp" alt="profile picture" class="pfp">
                        <span class="notification-sender-name">admin</span>
                    </span>
                    <span class="notification-bubble">
                        <div class="notification-arrow"></div>
                        You have been registered!
                    </span>
                </div>
            </section>

            <span class="nav-link" id="to_profile"><img src="../img/empty-pfp.webp" alt="profile picture"
                    class="pfp"></span>
            <span id="nav_username">Maksym Myna</span>

            <section id="profile-header-bar" class="not-visible">
                <div class="to-profile">
                    <a href="chat.php">Profile</a>
                </div>
                <div class="log-out">
                    <text class="logout">Log Out</text>
                </div>
            </section>
        </section>
    </header>
    <main>
        <nav class="chats-container">
            <div class="chats-header">
                <text> Chats </text>
                <button class="add-chat"><b>+</b></button>
            </div>
            <div class="chats-content">
                <div class="chats">
                </div>
            </div>
        </nav>
        <section class="messages-container">
            <div class="chat-header">Create or select chat</div>
            <div class="messages-content">
                <div class="messages">
                </div>
            </div>
            <section class="chat-footer not-visible">
                <blockquote contenteditable="plaintext-only" class="message-text"></blockquote>
                <button class="send"><svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000"
                        enable-background="new 0 0 1000 1000" xml:space="preserve">
                        <metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
                        <g>
                            <path fill="#fff"
                                d="M10,980.5l98.6-438.2l584.7-48.5l0.1-5.9l-583.6-32.7L23,19.5l967,467.4L10,980.5z" />
                        </g>
                    </svg>
                </button>
            </section>
        </section>

        <div class="popup_container not-visible">
            <div class="popup_container_content">
                <section class="right-top">
                    <button aria-label="close" class="exit-popup">
                        <svg id="close-adding" class="cross" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path d="M19 5L4.99998 19M5.00001 5L19 19" stroke="#000000" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </button>
                </section>
                <div class="user_info up">
                    <div class="user-info-content">
                        <form class="form">
                            <input type="text" class="search-input" name="login" id="login-signin" class="search"
                                placeholder="john.doe.pz.2023">
                            <button class="button" id="login" type="submit">Search</button>
                        </form>
                    </div>
                    <div class="login_error"></div>
                </div>
            </div>
        </div>

    </main>

    <script src="../../back/node/node_modules/socket.io/client-dist/socket.io.js"></script>
    <script src="../scripts/header.js"></script>
    <script src="../scripts/visibility.js"></script>
    <script src="../scripts/chat.js"></script>
    <script src="../scripts/search.js"></script>
</body>

</html>