if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./sw.js')
        .then(reg => reg)
        .catch(err => console.log('error', err))
}

let form = document.querySelector('#form')
let row = null
let currentPage = 1
let pageButtons = 1
let action = 'add'

let validationError = document.querySelector('#filling-mistake')
let removeWarning = document.querySelector('#remove-warning')
inputs = document.querySelectorAll('.user_info input, .user_info select')

users = []
let currentID = 2

function getUserIndex() {
    return currentPage * 4 - 4 + row.rowIndex - 1
}

let addingContainer = document.querySelectorAll('.popup_container')[0]
let studentsTable = document.querySelector('#students-table-body')

function addUserToTheTable(user) {
    let tr = document.createElement('tr')
    let tdCB = document.createElement('td')
    let cb = document.createElement('input')
    cb.type = 'checkbox'
    cb.name = 'select_row'
    cb.classList.toggle('checkbx')
    cb.id = `user${user.id}`
    cb.addEventListener('change', () => {
        if ((users.length >= 4 && document.querySelectorAll('.checkbx:checked').length === 4) ||
            (users.length < 4 && document.querySelectorAll('.checkbx:checked').length === users.length)) {
            document.querySelector('#selectAll').checked = true
        } else {
            document.querySelector('#selectAll').checked = false
        }
    })
    let label = document.createElement('label');
    label.htmlFor = `user${user.id}`;
    label.classList.toggle('visuallyhidden')
    tdCB.append(cb)

    let tdGroup = document.createElement('td')
    tdGroup.append(user.studentGroup)
    let tdName = document.createElement('td')
    tdName.append(`${user.firstName} ${user.lastName} `)
    let tdGender = document.createElement('td')
    tdGender.append(user.gender)
    let tdBirthday = document.createElement('td')
    tdBirthday.append(user.birthday)
    let tdStatus = document.createElement('td')
    tdStatus.append('⬤')
    tdStatus.classList.toggle('status')
    if (user.status)
        tdStatus.classList.toggle('status_active')
    let tdOptions = document.createElement('td')
    let editUser = document.createElement('button')
    editUser.setAttribute('title', 'Edit user');
    editUser.classList.add('edit-user')
    editUser.innerHTML = '<svg fill="#000000" class="pencil" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"> <path d="M4.07,14.927a.976.976,0,0,0-.167.25c-.011.023-.023.043-.032.067a.362.362,0,0,0-.019.04l-1.8,5.4A1,1,0,0,0,3,22a1.014,1.014,0,0,0,.316-.051l5.4-1.8c.013,0,.023-.013.036-.017s.041-.021.062-.031a.967.967,0,0,0,.259-.172c.01-.009.024-.012.033-.022l12.6-12.6a1,1,0,0,0,0-1.414l-3.6-3.6a1,1,0,0,0-1.414,0l-12.6,12.6C4.083,14.9,4.08,14.917,4.07,14.927ZM17.4,4.414,19.586,6.6,8.4,17.786,7.307,16.693,6.214,15.6ZM5.237,17.451l.656.656.656.656-1.968.656Z" /> </svg>'

    let removeUser = document.createElement('button')
    removeUser.setAttribute('title', 'Remove user');
    removeUser.classList.toggle('remove-user')
    removeUser.innerHTML = '<svg class="cross" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M19 5L4.99998 19M5.00001 5L19 19" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" /> </svg>'
    tdOptions.append(editUser, removeUser)
    tr.append(tdCB, tdGroup, tdName, tdGender, tdBirthday, tdStatus, tdOptions)
    tr.userID = user.id
    studentsTable.append(tr)

    let deleteButtons = document.querySelectorAll('.remove-user')
    deleteButtons[deleteButtons.length - 1].addEventListener('click', ev => {
        row = ev.target.closest('tr');
        makeVisible(removeWarning)
        let usersToDelete = document.querySelectorAll('.checkbx:checked')
        let names = usersToDelete.length < 1 ? row.cells[2].innerText :
            Array.from(usersToDelete).map(cb => cb.closest('tr').cells[2].innerText).toString()
        document.querySelector('.warning-text').innerText = `Are you sure you want to delete ${names}?`
    })

    let editButtons = document.querySelectorAll('.edit-user')
    editButtons[editButtons.length - 1].addEventListener('click', ev => {
        document.querySelector('#popup-title').innerText = 'Edit Student'
        action = 'edit'
        row = ev.target.closest('tr')

        user = users[getUserIndex()]
        inputs[0].value = user.studentGroup
        inputs[1].value = user.firstName
        inputs[2].value = user.lastName
        inputs[3].value = user.gender === 'M' ? 'male' : user.gender === 'F' ? 'female' : user.gender === 'O' ? 'other' : user.gender
        inputs[4].value = user.birthday

        for (let i = 0; i != form.length; ++i) {
            validation[i] = true
            checkSVG(inputChecks[i])
        }
        makeVisible(addingContainer)
    })
}

function addUser(user) {
    users.push(user);
    if (document.querySelectorAll('tbody tr').length < 4) {
        addUserToTheTable(user)
    }

    if (pageButtons != 5 && parseInt((users.length - 1) / 4) == pageButtons) {
        nextPage = document.createElement('text')
        nextPage.append(++pageButtons)
        document.querySelector('.table_navigation text:last-of-type').after(nextPage)
        nextPage.addEventListener('click', ev => {
            addNumberNavigation(ev.target)
        })
    }
    if (pageButtons > 1) {
        let navigationArrows = document.querySelectorAll('.table_navigation svg')
        navigationArrows[2].classList.remove('disabled-svg')
        navigationArrows[3].classList.remove('disabled-svg')
    }
}

function printError(error) {
    validationError.innerText = error;
}

document.querySelector('#ok-add-user').addEventListener('click', event => {
    event.target.disabled = true
    if (action == 'add') {
        addUserToServer(event.target)
    } else if (action == 'edit') {
        editUser(event.target)
    }
})

function addUserToServer(button) {
    for (let i = 0; i != validation.length; ++i)
        if (!validation[i]) {
            printError('Fill the fields correctly!')
            button.disabled = false
            return
        }

    addUserOnServer(form).then(resp => {
        if (resp.hasOwnProperty('error')) {
            printError(resp.error)
            button.disabled = false
            return;
        }
        addUser(new User(resp.group, resp.first_name, resp.last_name, resp.gender, resp.birthday))
        hidePopUp(addingContainer)
    }).catch(err => printError(err))
}

function editUser(button) {
    editUserOnServer(form).then(resp => {
        if (resp.hasOwnProperty('error')) {
            printError(resp.error)
            button.disabled = false
            return;
        }

        user = users[getUserIndex()]
        user.studentGroup = resp.group
        user.firstName = resp.first_name
        user.lastName = resp.last_name
        user.gender = resp.gender === 'male' ? 'M' : resp.gender === 'female' ? 'F' : 'O'
        user.birthday = resp.birthday
        editUserInRow(user, row)
        hidePopUp(addingContainer)
    }).catch(err => printError(err))
}

function editUserInRow(user, row) {
    row.cells[1].innerText = user.studentGroup
    row.cells[2].innerText = `${user.firstName} ${user.lastName} `
    row.cells[3].innerText = user.gender
    row.cells[4].innerText = user.birthday
}

function removeUser() {
    let id = row.userID
    removeUserFromServer(id)
    users.splice(getUserIndex(), 1)
    if (document.querySelectorAll('tbody tr').length === 1 && currentPage !== 1) {
        document.querySelector('.current_tab').remove();
        document.querySelector('.table_navigation text:last-of-type').classList.toggle('current_tab')
        loadTablePage(currentPage - 1)
    }
    row.remove()
    if (currentPage !== getLastPage() && document.querySelectorAll('tbody tr').length !== 4) {
        addUserToTheTable(users[currentPage * 4 - 1])
    }
    loadTab()
}

document.querySelector('#ok-delete-user').addEventListener('click', () => {
    let checkedBoxes = document.querySelectorAll('input[type="checkbox"]:checked')
    if (!checkedBoxes.length) {
        removeUser()
    } else {
        Array.from(checkedBoxes).filter(checkbox => checkbox.name !== 'select_all').forEach(checkbox => {
            row = checkbox.closest('tr');
            removeUser()
        })
        document.querySelector('input[type="checkbox"]').checked = false
    }
    makeInvisible(removeWarning)
})


getUsersFromServer().then(resp => {
    if (!resp.length)
        return;

    resp.forEach(user => addUser(User.createUser(user)))
    currentID = parseInt(resp[resp.length - 1].id) + 1;
}).catch(err => printError(err))