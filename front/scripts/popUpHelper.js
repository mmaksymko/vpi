function hidePopUp(popUp) {
    makeInvisible(popUp)
    setTimeout(() => {
        form.reset()
        for (let i = 0; i != form.length; ++i) {
            if (!form[i].value) {
                uncheckSVG(inputChecks[i])
                validation[i] = false
            } else {
                checkSVG(inputChecks[i])
            }
        }
        document.querySelector('#ok-add-user').disabled = false
    }, 250)
    printError('')
    action = "add"
}


document.querySelector('#cancel-delete-user').addEventListener('click', () => makeInvisible(removeWarning))
document.querySelector('#exit-deletion').addEventListener('click', () => makeInvisible(removeWarning))

document.querySelector('.user_adding').addEventListener('click', event => {
    document.querySelector('#popup-title').innerText = 'Add Student'
    makeVisible(addingContainer)
})
document.querySelector('.exit-popup').addEventListener('click', () => {
    hidePopUp(addingContainer)
})
document.querySelector('#cancel-add-user').addEventListener('click', () => {
    hidePopUp(addingContainer)
})
