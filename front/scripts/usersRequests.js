async function getUsersFromServer() {
    const response = await fetch('../../back/php/users.php', { method: 'GET' });
    try {
        return await response.json();
    } catch (e) {
        return printError(`Error response code: ${response.status} `)
    }
}

async function addUserOnServer(userForm) {
    let formData = new FormData(userForm);
    formData.append("id", currentID);

    const response = await fetch('../../back/php/users.php', {
        method: 'POST',
        body: formData
    });

    try {
        return await response.json();
    } catch (e) {
        return printError(`Error response code: ${response.status} `)
    }
}

async function editUserOnServer(userForm) {
    let formData = new FormData(userForm);
    formData.append("id", row.userID);

    const response = await fetch('../../back/php/users.php', {
        method: 'PUT',
        body: JSON.stringify(Object.fromEntries(formData))
    });

    try {
        return await response.json();
    } catch (e) {
        return printError(`Error response code: ${response.status} `)
    }
}

async function removeUserFromServer(id) {
    const response = await fetch('../../back/php/users.php', {
        method: 'DELETE',
        body: JSON.stringify({ "id": id })
    });

    try {
        let resp = await response.json();
        if (!resp.hasOwnProperty('success'))
            throw null
    } catch (e) {
        return printError(`Error response code: ${response.status} `)
    }
}
