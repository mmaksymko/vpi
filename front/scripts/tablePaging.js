let prevPage = 1
let lastPage = getLastPage()
let navigationArrows = document.querySelectorAll('.table_navigation svg')

function getLastPage() {
    return parseInt((users.length - 1) / 4) + 1
}

function selectCurrentTab() {
    if (currentPage == prevPage)
        return

    lastPage = getLastPage()
    let anchorTags = document.querySelectorAll('.table_navigation text')

    studentsTable.innerHTML = ''
    if (currentPage !== lastPage)
        for (let i = 0; i != 4; ++i)
            addUserToTheTable(users[4 * currentPage - 4 + i])
    else
        for (let i = 0; i < users.length && 4 * currentPage - 4 + i < users.length && i != users.length; ++i)
            addUserToTheTable(users[4 * currentPage - 4 + i])

    document.querySelector('.current_tab').classList.toggle('current_tab')

    if (currentPage === 1) {
        let page = 1
        anchorTags.forEach(anchor => anchor.innerText = page++)
        anchorTags[0].classList.toggle('current_tab')
    } else if (currentPage === lastPage) {
        for (let i = anchorTags.length - 1; i >= 0; --i) {
            anchorTags[i].innerText = currentPage - anchorTags.length + 1 + i
            anchorTags[anchorTags.length - 1].classList.add('current_tab')
        }
    } else if (currentPage !== prevPage) {
        let atEndLimit = parseInt(anchorTags[anchorTags.length - 1].innerText) === lastPage
        let atStartLimit = parseInt(anchorTags[0].innerText) === 1
        anchorTags.forEach(anchor => {
            let value = parseInt(anchor.innerText)
            if (currentPage < prevPage && !atStartLimit) {
                value--
            } else if (currentPage > prevPage && !atEndLimit) {
                value++
            }
            anchor.innerText = value
            if (value === currentPage) {
                anchor.classList.toggle('current_tab')
            }
        })
    }
}

function loadTablePage(page) {
    prevPage = currentPage
    currentPage = page
    lastPage = parseInt((users.length - 1) / 4) + 1

    if (page !== 1) {
        navigationArrows[0].classList.remove('disabled-svg')
        navigationArrows[1].classList.remove('disabled-svg')
    } else {
        navigationArrows[0].classList.add('disabled-svg')
        navigationArrows[1].classList.add('disabled-svg')
    }

    if (lastPage !== page) {
        navigationArrows[2].classList.remove('disabled-svg')
        navigationArrows[3].classList.remove('disabled-svg')
    } else {
        navigationArrows[2].classList.add('disabled-svg')
        navigationArrows[3].classList.add('disabled-svg')
    }

    selectCurrentTab()
}

function addNumberNavigation(target) {
    loadTablePage(parseInt(target.innerText))
}

document.querySelector('.table_navigation text').addEventListener('click', ev => {
    addNumberNavigation(ev.target)
})

navigationArrows[0].addEventListener('click', () => {
    if (currentPage !== 1)
        loadTablePage(1)
})
navigationArrows[1].addEventListener('click', () => {
    if (currentPage !== 1)
        loadTablePage(currentPage - 1)
})
navigationArrows[2].addEventListener('click', () => {
    if (currentPage !== parseInt((users.length - 1) / 4) + 1)
        loadTablePage(currentPage + 1)
})
navigationArrows[3].addEventListener('click', ev => {
    if (currentPage !== parseInt((users.length - 1) / 4) + 1) {
        loadTablePage(parseInt((users.length - 1) / 4) + 1)
    }
})

function loadTab() {
    rows = document.querySelectorAll('tbody tr');
    rows.forEach(row => row.remove())

    for (let i = currentPage * 4 - 4; i < users.length && i != currentPage * 4; ++i) {
        addUserToTheTable(users[i])
    }

    let anchorTags = document.querySelectorAll('.table_navigation text')
    let rowsNumber = parseInt(anchorTags[anchorTags.length - 1].innerText) * 4 - 3
    if (rowsNumber !== 0 && users.length < rowsNumber && pageButtons !== 1) {
        anchorTags[anchorTags.length - 1].remove()
        pageButtons--
        loadTablePage(currentPage)
    }
}

document.querySelector('.user_check').addEventListener('change', () => {
    this.checked = !this.checked
    let checkboxes = document.querySelectorAll('input[type=checkbox]')
    for (let i = 0; i != checkboxes.length; ++i) {
        checkboxes[i].checked = this.checked
    }
})