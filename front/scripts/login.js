async function logIn(formData) {
    const response = await fetch('../../back/php/checkLogIn.php', {
        method: 'POST',
        body: formData
    });

    try {
        return await response.json()
    } catch (e) {
        return { 'error': e }
    }
}

function capitalizeFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

document.querySelector('form').addEventListener('submit', event => {
    let formData = new FormData(event.target)
    logIn(formData).then(resp => {
        if (resp.hasOwnProperty('error')) {
            document.querySelector('.login_error').innerText = resp.error
            return;
        }
        login = capitalizeFirst(resp.firstName) + ' ' + capitalizeFirst(resp.lastName)
        document.querySelector('.login_error').innerText = ''
        sessionStorage.setItem('login', login);
        sessionStorage.setItem('user', resp.username);
        location.reload()
    })

    event.preventDefault();
})