document.querySelector('#nav_username').innerHTML = sessionStorage.getItem('login')
document.querySelector('.log-out').addEventListener('click', event => {
    logOut()
})

async function logOut() {
    await fetch('../../back/php/logOut.php');
    location.reload()
}

let notificationIcon = document.querySelector(".bell")
let notificationBar = document.querySelector("#notification-bar")
let profileHeaderIcon = document.querySelector("#to_profile")
let profileHeaderBar = document.querySelector("#profile-header-bar");

function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

function addPopUpListeners(array) {
    let hovers = [false, false];

    for (let i = 0; i != array.length; ++i) {
        array[i].addEventListener('mouseover', () => {
            hovers[i] = true
            makeVisible(array[1])
        })
        array[i].addEventListener('mouseout', () => {
            hovers[i] = false
            delay(150).then(() => {
                if (!hovers[0] && !hovers[1])
                    makeInvisible(array[1])
            })
        })
    }
}

addPopUpListeners([profileHeaderIcon, profileHeaderBar])
addPopUpListeners([notificationIcon, notificationBar])