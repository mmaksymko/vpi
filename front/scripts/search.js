let popup = document.querySelector('.popup_container')

async function search(formData) {
    const response = await fetch('../../back/php/search.php', {
        method: 'POST',
        body: formData
    });

    try {
        return await response.json()
    } catch (e) {
        return { 'error': e }
    }
}

async function addChat(username) {
    const response = await fetch('http://localhost:4001/chat/', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "user1": username,
            "user2": sessionStorage.getItem('user')
        })
    })
    return response.json()
}

document.querySelector('form').addEventListener('submit', event => {
    let formData = new FormData(event.target)
    formData.append("username", sessionStorage.getItem('user'));

    search(formData).then(resp => {
        if (resp.hasOwnProperty('error')) {
            document.querySelector('.login_error').innerText = resp.error
            return;
        }
        let name = resp.firstName + ' ' + resp.lastName

        addChat(resp.username)
        displayChat(name, resp.username)
        makeInvisible(popup)
    })

    event.preventDefault();
    event.target.reset()
})

document.querySelector('.add-chat').addEventListener('click', e => {
    if (e.target.innerHTML === '&gt;' || e.target.innerHTML === '<b>&gt;</b>') {
        unselectChat()
        resize()
    } else {
        makeVisible(popup)
    }
})
document.querySelector('.exit-popup').addEventListener('click', () => makeInvisible(popup))
