let socket = io('http://localhost:4001');
let room = null
MessageType = { Sent: 'sent', Recieved: 'received' }

let messagesContainer = document.querySelector('.messages-content')
let messagesDiv = document.querySelector('.messages')
let chatsDiv = document.querySelector('.chats')
let sendButton = document.querySelector('.send')

socket.on('receive', (msg) => {
    var audio = new Audio('../audio/notification.mp3');
    audio.volume = 1
    audio.play();
    displayMessage(MessageType.Recieved, msg.text, msg.time)
    scroll(messagesContainer)
})

function displayMessage(type, text, time) {
    if (type !== MessageType.Sent && type !== MessageType.Recieved) return

    let messageContainer = document.createElement('section')
    messageContainer.classList.toggle('message-container')
    messageContainer.classList.toggle(type)

    let message = document.createElement('div')
    message.append(text);
    message.classList.toggle('message')

    let timestamp = document.createElement('legend')
    timestamp.append(time)
    timestamp.classList.toggle('time')

    messageContainer.append(message)
    messageContainer.append(timestamp)
    messagesDiv.append(messageContainer)
}

function displayChat(name, username) {
    let chat = document.createElement('div')

    let nameText = document.createElement('text')
    nameText.append(name.split(' ').map(word => capitalizeFirst(word)).join(' '))

    let usernameText = document.createElement('text')
    usernameText.classList.toggle('receiver-username')
    usernameText.append(`@${username}`)

    chat.append(nameText, usernameText);
    chat.classList.toggle('chat')

    chat.addEventListener('click', () => selectChat(chat))

    chatsDiv.append(chat)
}

let unselectChat = (chat) => {
    document.querySelector('.selected')?.classList.toggle('selected');
    messagesDiv.innerHTML = ""
    document.querySelector('.chat-header').innerText = 'Create or select chat'
    makeInvisible(document.querySelector('.chat-footer'))
}

let selectChat = (chat) => {
    if (!document.querySelector('.chat')) {
        return
    }
    if (document.querySelector('.selected'))
        document.querySelector('.selected').classList.toggle('selected');
    else
        makeVisible(document.querySelector('.chat-footer'))

    document.querySelector('.chat-header').innerText = chat.childNodes[0].innerText
    chat.classList.toggle('selected');
    sessionStorage.setItem('receiver', chat.childNodes[1].innerText.split('@')[1]);
    messagesDiv.innerHTML = ""
    openChat(sessionStorage.getItem('receiver'), sessionStorage.getItem('user'))
        .then(res => {
            sessionStorage.setItem('roomID', res[0]?._id || null)
            res[0]?.messages?.forEach(message => {
                displayMessage(
                    (message.sender === sessionStorage.getItem('user')) ? MessageType.Sent : MessageType.Recieved,
                    message.body,
                    message.time)
                scroll(messagesContainer)
            })
        })

    room = sessionStorage.getItem('user') > sessionStorage.getItem('receiver') ? `${sessionStorage.getItem('user')} / ${sessionStorage.getItem('receiver')}` : `${sessionStorage.getItem('receiver')} / ${sessionStorage.getItem('user')}`
    socket.emit("join-chat", room)
    resize()
}

document.querySelectorAll('.chat').forEach(chat => chat.addEventListener('click', () => selectChat(chat)))

sendButton.addEventListener('click', (event) => {
    let text = document.querySelector('.message-text').innerText.replace(/^\s+|\s+$/g, '')
    if (!text || text.match(/^ *$/)) return

    socket
        .emit('send', {
            'sender': sessionStorage.getItem('login'),
            'receiver': sessionStorage.getItem('receiver'),
            'text': text,
            'time': getTime()
        }, room);

    addMessage(sessionStorage.getItem('roomID'), sessionStorage.getItem('user'), text, getTime()).then()
    displayMessage(MessageType.Sent, text, getTime())

    document.querySelector('.message-text').innerText = ""
    scroll(messagesContainer)
})

document.querySelector('.message-text').addEventListener('keydown', e => {
    if ((e.metaKey || e.ctrlKey)) {
        if (e.keyCode === 13) {
            if (e.target.lastChild.localName !== 'br')
                e.target.innerText += "\n"
            e.target.innerText += "\n"
            let range = document.createRange();
            range.selectNodeContents(e.target);
            range.collapse(false);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
            scroll(e.target)
        }
    } else if (e.keyCode === 13) {
        sendButton.click()
        event.preventDefault()
    }
})

selectChat(document.querySelector('.chat'))

async function getChats(username) {
    const response = await fetch(`http://localhost:4001/chat/?`
        + new URLSearchParams({
            'username': username
        }),
        {
            method: 'GET'
        })
    if (response.status.toString[0] === '4') alert(`${response.statusText}`)
    return response.json()
}

async function openChat(user1, user2) {
    const response = await fetch(`http://localhost:4001/chat/open?`
        + new URLSearchParams({
            'user1': user1,
            'user2': user2
        }),
        {
            method: 'GET'
        })
    if (response.status.toString[0] === '4') alert(`${response.statusText}`)
    return response.json()
}

async function addMessage(id, sender, body, time) {
    const response = await fetch(`http://localhost:4001/chat/${id}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "sender": sender,
            "body": body,
            "time": time
        })
    })
    if (response.status.toString[0] === '4') alert(`${response.statusText}`)
    return response.json()
}

getChats(sessionStorage.getItem('user')).
    then(resp => resp.forEach(obj =>
        displayChat(parseUser(obj.user1 === sessionStorage.getItem('user') ? obj.user2 : obj.user1), obj.user1 === sessionStorage.getItem('user') ? obj.user2 : obj.user1)))

function parseUser(user) {
    let split = user.split('.')
    return (split.length >= 2) ? `${split[0]} ${split[1]}` : split[0]
}

function getTime() {
    let date = new Date()
    let hours = !parseInt(date.getHours() / 10) ? `0${date.getHours()}` : date.getHours()
    let minutes = !parseInt(date.getMinutes() / 10) ? `0${date.getMinutes()}` : date.getMinutes()
    return `${hours}:${minutes}`
}

function scroll(element) {
    element.scrollTop = element.scrollHeight
}

function capitalizeFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

window.addEventListener('resize', resize, true);

function resize() {
    let width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth

    document.querySelector('.chats-container').classList.remove('width-seventy')
    document.querySelector('.chats-container').classList.remove('width-zero')
    document.querySelector('.messages-container').classList.remove('width-zero')
    document.querySelector('.messages-container').classList.remove('width-eighty')
    document.querySelector('.chats-content').classList.remove('width-zero')
    makeVisible(document.querySelector('.chats-content'))
    makeVisible(document.querySelector('.chats-header').childNodes[1])
    document.querySelector('.chats-container').classList.remove('width-small')
    document.querySelector('.chats-header').childNodes[3].innerHTML = "<b>+</b>"

    if (width > 600) return

    if (!this.document.querySelector('.selected')) {
        document.querySelector('.chats-container').classList.add('width-seventy')
        document.querySelector('.messages-container').classList.add('width-zero')
    } else {
        document.querySelector('.messages-container').classList.add('width-eighty')
        document.querySelector('.chats-container').classList.add('chats-small')

        makeInvisible(document.querySelector('.chats-content'))
        makeInvisible(document.querySelector('.chats-header').childNodes[1])
        document.querySelector('.chats-container').classList.add('width-small')
        document.querySelector('.chats-header').childNodes[3].innerHTML = "<b>></b>"
    }
}
resize()