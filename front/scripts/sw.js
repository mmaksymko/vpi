const staticCacheName = 'static-cache-v6'
const assets = [
    // '/',
    // '/index.html',
    // '/style.css',
    // '/app.js',
    // '/ui.js',
    // 'site.php',
    '../img/icon-72x72.png',
    '../img/icon-96x96.png',
    '../img/icon-128x128.png',
    '../img/icon-144x144.png',
    '../img/icon-152x152.png',
    '../img/icon-192x192.png',
    '../img/icon-384x384.png',
    '../img/icon-512x512.png'
]

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                cache.addAll(assets)
            })
    )
})

self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(keys => {
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key))
            )
        })
    )
})

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request).then(response => {
            return response || fetch(event.request)
        })
    )
})
