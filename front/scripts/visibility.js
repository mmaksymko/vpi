function makeInvisible(element) {
    element.classList.remove('visible')
    element.classList.add('not-visible')
}

function makeVisible(element) {
    element.classList.remove('not-visible')
    element.classList.remove('not-visible-no-animation')
    element.classList.add('visible')
}