class User {
    static createUser(user) {
        return new User('', '', '', '', '', false, user)
    }

    constructor(studentGroup = '', firstName = '', lastName = '', gender = '', birthday = '', status = false, user = null) {
        if (user != null) {
            this.studentGroup = user.studentGroup
            this.firstName = user.firstName
            this.lastName = user.lastName
            gender = user.gender
            this.birthday = user.birthday
            this.id = user.id;
        } else {
            this.studentGroup = studentGroup
            this.firstName = firstName
            this.lastName = lastName
            this.birthday = birthday
            this.id = currentID++
        }

        this.userName = `${this.firstName}.${this.lastName}.${this.studentGroup.slice(0, this.studentGroup.indexOf('-'))}.${new Date().getFullYear()}`.toLowerCase()

        if (gender === 'male')
            this.gender = 'M'
        else if (gender === 'female')
            this.gender = 'F'
        else
            this.gender = 'O'
    }
}