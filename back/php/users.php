<?php
include('controllers/users_controller.php');

$request = $_SERVER['REQUEST_METHOD'];

if ($request === 'POST') {
    addUser($_POST);
}

if ($request === 'GET') {
    echo getUsers();
}

if ($request === 'PUT') {
    editUser(json_decode(file_get_contents('php://input'), true));
}

if ($request === 'DELETE') {
    removeUser(json_decode(file_get_contents('php://input'), true));
}

mysqli_close($conn);