<?php
include('database_connection.php');

function addUser($user)
{
    global $conn;
    $user = checkData($user);
    $user['password'] = password_hash(str_replace('-', '.', date("d-m-Y", strtotime($user['birthday']))), PASSWORD_BCRYPT);
    $user['username'] = strtolower($user['first_name'] . '.' . $user['last_name'] . '.' . substr($user['group'], 0, strpos($user['group'], '-')) . '.' . date("Y"));

    $sql = "INSERT INTO users (id, studentGroup, firstName, lastName, gender, birthday, username, type, password)
            VALUES ($user[id], '$user[group]', '$user[first_name]', '$user[last_name]', '$user[gender]', '$user[birthday]', '$user[username]', 'user', '$user[password]')";

    $result = $conn->query($sql);
    if ($result != TRUE) {
        die(json_encode(array("error" => $conn->error)));
    }

    echo json_encode($user);
}
function getUsers()
{
    global $conn;
    $result = $conn->query("SELECT * FROM users WHERE type != 'admin'");

    $json_array = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $json_array[] = $row;
    }

    echo json_encode($json_array);
}

function editUser($data)
{
    global $conn;
    $user = checkData($data);

    $sql = "UPDATE users
            SET studentGroup = '$user[group]', firstName = '$user[first_name]', lastName = '$user[last_name]',
                gender = '$user[gender]', birthday = '$user[birthday]'
            WHERE id = $user[id]";

    $result = $conn->query($sql);

    if ($result != TRUE) {
        die(json_encode(array("error" => $conn->error)));
    }

    echo json_encode($user);
}

function removeUser($data)
{
    global $conn;
    $id = $data['id'];

    $sql = "DELETE FROM users WHERE id = $id";

    $result = $conn->query($sql);
    if ($result != TRUE) {
        die(json_encode(array("error" => $conn->error)));
    }

    echo json_encode(array("success" => "hurray!"));
}

function checkData($request)
{
    $user = array(
        "group" => test_input($request["group"]),
        "first_name" => test_input(ucfirst($request["first_name"])),
        "last_name" => test_input(ucfirst($request["last_name"])),
        "gender" => test_input($request["gender"]),
        "birthday" => test_input($request["birthday"]),
        "id" => $request["id"]
    );

    if (empty($user["group"]) || empty($user["first_name"]) || empty($user["last_name"]) || empty($user["gender"]) || empty($user["birthday"])) {
        die(json_encode(array("error" => "Fill all the fields")));
    } else if (!preg_match('/^\p{L}+$/u', $user["first_name"]) || !preg_match('/^\p{L}+$/u', $user["last_name"])) {
        die(json_encode(array("error" => "Name can only contain letters")));
    } else if (strlen($user["first_name"]) > 35 || strlen($user["last_name"]) > 35) {
        die(json_encode(array("error" => "Max length is 35 characters")));
    } else if (!testDate($user["birthday"])) {
        die(json_encode(array("error" => "Enter the correct date")));
    }
    return $user;
}

function testDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date && strtotime($date) <= strtotime('now');
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>