<?php
include('database_connection.php');

function search($data)
{
    global $conn;
    $result = $conn->query("SELECT * FROM users WHERE username='$data[login]'");

    if ($result->num_rows != 0) {
        $result = mysqli_fetch_assoc($result);
        $user['firstName'] = $result['firstName'];
        $user['lastName'] = $result['lastName'];
        $user['username'] = $result['username'];

        if ($user['username'] === $data['username'])
            die(json_encode(array("error" => "Chating with yourself is not yet supported!")));
        die(json_encode($user));
    }
    die(json_encode(array("error" => "Such user doesn't exist!")));
}
?>