<?php
include('database_connection.php');

function checkLoginData($data)
{
    global $conn;
    $result = $conn->query("SELECT * FROM users WHERE username='$data[login]'");

    if ($result->num_rows != 0) {
        $user = mysqli_fetch_assoc($result);
        if (password_verify($data['password'], $user['password'])) {
            setcookie("type", $user['type'], time() + 3600, '/');
            die(json_encode($user));
        }
        die(json_encode(array("error" => "Wrong password!")));
    }
    die(json_encode(array("error" => "Wrong login!")));

}
?>