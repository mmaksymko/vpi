const mongoose = require('mongoose')
const Chat = require('../models/chat.js')

function sortUsers(obj) {
    let user1 = obj.user1 > obj.user2 ? obj.user2 : obj.user1
    let user2 = user1 === obj.user1 ? obj.user2 : obj.user1
    return { "user1": user1, "user2": user2 }
}

const createChat = async (req, res) => {
    let { user1, user2 } = sortUsers(req.body)
    let chat = new Chat({
        "user1": user1,
        "user2": user2,
        "messages": []
    })

    await chat.save().then(user => res.status(200).json(JSON.stringify(user))).catch(err => res.status(401))
}

const getChats = (req, res) => {
    Chat.find({
        $or: [
            { "user1": req.query.username },
            { "user2": req.query.username }
        ]
    }).then(result => res.status(200).send(result))
        .catch(res.status(401))
}

const openChat = (req, res) => {
    let { user1, user2 } = sortUsers(req.query)

    Chat.find({
        $and: [
            { "user1": user1 },
            { "user2": user2 }
        ]
    }).then(result => res.status(200).send(result))
        .catch(res.status(401))
}

const addMessage = (req, res) => {
    const body = req.body
    Chat.updateOne(
        {
            _id: req.params.id
        }, {
        $push: {
            messages: {
                sender: body.sender,
                body: body.body,
                time: body.time
            }
        }
    }).then(result => res.status(200).send(result))
        .catch(err => res.status(401))
}

module.exports = {
    createChat,
    getChats,
    addMessage,
    openChat
}