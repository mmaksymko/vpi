const mongoose = require('mongoose')

let chatSchema = new mongoose.Schema({
    user1: { type: String, required: true },
    user2: { type: String, required: true },
    messages: [
        {
            sender: { type: String, required: true },
            body: { type: String, required: true },
            time: { type: String, required: true }
        }
    ]
})

chatSchema.index({ user1: 1, user2: 1 }, { "unique": true });
const Chat = mongoose.model('Chat', chatSchema)

module.exports = Chat