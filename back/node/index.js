const express = require('express')
const app = express()
const port = 4001
const http = require('http').Server(app)
const cors = require('cors')
app.use(cors())
app.use(express.json());

const io = require("socket.io")(http, {
    cors: {
        origin: ["http://localhost:4000"]
    }
})

io.on('connection', (socket) => {
    socket.on('send', (msg, room) => {
        socket.to(room).emit('receive', msg)
    });
    socket.on('join-chat', room => {
        socket.join(room)
    })
});

const chatRouter = require('./routes/chatRouter')
app.use('/chat', chatRouter)

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/chat', { useNewUrlParser: true, useUnifiedtopology: true })
    .then()
    .catch(err => console.log(err))

http.listen(port, () => { })