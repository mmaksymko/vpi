const express = require("express")
const router = express.Router()
const chatController = require('../controllers/chatController')

router.route('/')
    .post(chatController.createChat)
    .get(chatController.getChats)

router.route('/:id')
    .post(chatController.addMessage)

router.route('/open')
    .get(chatController.openChat)

module.exports = router